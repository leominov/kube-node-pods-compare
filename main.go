package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	agoFilter = flag.Duration("ago", 0, "Max ago")
)

func main() {
	err := realMain()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func realMain() error {
	flag.Parse()

	cli, err := NewClientSet()
	if err != nil {
		return err
	}

	var replicaSets []*ReplicaSet
	payloadMap := make(map[string]map[string]bool)

	for _, node := range flag.Args() {
		if _, ok := payloadMap[node]; !ok {
			payloadMap[node] = make(map[string]bool)
		}
		podList, err := cli.CoreV1().Pods("").List(context.Background(), metav1.ListOptions{
			FieldSelector: fmt.Sprintf("spec.nodeName=%s", node),
		})
		if err != nil {
			fmt.Println(err)
			continue
		}
		for _, pod := range podList.Items {
			if len(pod.OwnerReferences) == 0 {
				continue
			}
			owner := pod.OwnerReferences[0]
			if owner.Kind != "ReplicaSet" {
				continue
			}
			rs := &ReplicaSet{
				Name:      owner.Name,
				Namespace: pod.Namespace,
			}
			if _, ok := payloadMap[node][rs.String()]; !ok {
				replicaSets = append(replicaSets, rs)
				payloadMap[node][rs.String()] = true
			}
		}
	}

	for _, rs := range replicaSets {
		match := true
		for _, node := range flag.Args() {
			if _, ok := payloadMap[node][rs.String()]; !ok {
				match = false
				break
			}
		}
		if match {
			replicaSet, err := cli.AppsV1().ReplicaSets(rs.Namespace).Get(context.TODO(), rs.Name, metav1.GetOptions{})
			if err != nil {
				fmt.Println(err)
				continue
			}
			ago := time.Now().Sub(replicaSet.CreationTimestamp.Time)
			if *agoFilter != 0 && ago > *agoFilter {
				continue
			}
			fmt.Printf("%s (replicas=%d, created=%s)\n", rs, replicaSet.Status.Replicas, ago.String())
		}
	}

	return nil
}
