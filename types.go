package main

import "fmt"

type ReplicaSet struct {
	Name      string
	Namespace string
}

func (r *ReplicaSet) String() string {
	return fmt.Sprintf("%s/%s", r.Namespace, r.Name)
}
