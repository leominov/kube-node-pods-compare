# kube-node-pods-compare

Поиск одинаковой нагрузки на указанных нодах.

## Применение

```shell
$ go run . gke-kube-pltf-prod-app-53b04c08-ybjr gke-kube-pltf-prod-app-8bdfa8d8-0c5g gke-kube-pltf-prod-app-6dfb5c72-lzqw
```

## Пример отчета

```text
app-crm-task-manager/crm-task-manager-69c6bbb4cc (replicas=3)
app-web-widget-footer/web-widget-footer-6bb95d8bc8 (replicas=5)
app-crm-task-manager/crm-task-manager-69c6bbb4cc (replicas=3)
app-web-widget-footer/web-widget-footer-6bb95d8bc8 (replicas=5)
app-crm-task-manager/crm-task-manager-69c6bbb4cc (replicas=3)
app-web-widget-footer/web-widget-footer-6bb95d8bc8 (replicas=5)
```
